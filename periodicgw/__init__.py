# -*- coding: utf-8 -*-
from .periodicgw_utils import  DomainData, RandomKField, solve_q, plot_lognormal_field, plot_quiver
__version__ = '1.0'
__author__ = 'Scott K. Hansen, Lian Zhou'
