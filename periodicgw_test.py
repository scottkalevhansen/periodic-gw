# -*- coding: utf-8 -*-
"""
periodic_gw_test.py.

This file generates a 2D random, doubly-periodic, locally log-normal hydraulic random conductivity field with specified
statistics and saves it. It subsequently opens this K field and solve the spatially periodic, heterogeneous Darcy flux field on the conductivity field we create with a specified mean flow angle and unit mean flux.
"""
from numpy import pi
from pickle import dump, load
from periodicgw import DomainData, RandomKField, solve_q, plot_lognormal_field, plot_quiver

# Generate a random, periodic, locally Gaussian conductivity field with user-difined geological spatial statistics (variance, correlation length and anisotropy). 
dd = DomainData(nr=50, nc=100, dr=2, dc=2)
K = RandomKField(dd, model_ref = 2, var =.5, corr_len = dd.num_cols*dd.dc/20, f_lambda =.5, azimuth=0*pi/2)
plot_lognormal_field(K)
with open('k_field_test.pickle',"wb") as dump_file:
    dump(K, dump_file)

# Read a previously saved conductivity field with exponential covariance
# domain size = 100*200, grid-cell size =2, variance=0.5, correlation length in the major and minor range of anisotropy is 10 and 5, azimuth angle=0
with open("k_field_test.pickle", "rb") as read_file:
    saved_K = load(read_file)

# Solve the velocity field on the saved conductivity field          
q_r, q_c = solve_q(saved_K, angle=0)
plot_quiver(saved_K, q_r, q_c) 